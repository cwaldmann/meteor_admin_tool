describe('AdminUserService', function () {
    'use strict';

    describe('validateUserData', function () {
        it('when no username entered return FALSE', function () {
            expect(AdminUserService.validateUserData('', '123', 'test@test.de')).toBe(false);
        });

        it('when no password entered return FALSE', function () {
            expect(AdminUserService.validateUserData('username', '', 'test@test.de')).toBe(false);
        });

        it('when no email entered return FALSE', function () {
            expect(AdminUserService.validateUserData('username', '123', '')).toBe(false);
        });


        it('when no username and no email entered return FALSE', function () {
            expect(AdminUserService.validateUserData('', '', 'test@test.de')).toBe(false);
        });


        it('when no password and no email entered return FALSE', function () {
            expect(AdminUserService.validateUserData('username', '', '')).toBe(false);
        });

        it('full transmitted data should return TRUE', function () {
            expect(AdminUserService.validateUserData('username', '123', 'email')).toBe(true);
        });
    });



    describe('addNewUser', function () {
        it('when there are some missing fields there should be an error', function () {
            AdminUserService.addNewUser('username', '', '', function (err) {
                expect(err).toBeTruthy();
            });
        });

        it('when there are some missing fields the User add function should not be called', function () {
            AdminUserService.addNewUser('username', '', '', function (err) {
                expect(err).toBeTruthy();
            });
        });

        it('when the user data is correct, a new user should be added', function () {
            spyOn(Meteor, 'call');

            AdminUserService.addNewUser('username', '123', 'email', function (err) {
                expect(Meteor.call).not.toHaveBeenCalled();
            });
        });
    });



    describe('deleteUser', function () {
        it('when there is no ID the remove function should not be called', function () {
            spyOn(Meteor, 'call');

            AdminUserService.deleteUser();
            expect(Meteor.call).not.toHaveBeenCalled();
        });

        it('when there is no callback function the remove function should not be called', function () {
            spyOn(Meteor, 'call');

            AdminUserService.deleteUser('id');
            expect(Meteor.call).not.toHaveBeenCalled();
        });

        it('when there is an ID and a callback function the remove function should called', function () {
            spyOn(Meteor, 'call');

            AdminUserService.deleteUser('id', function () {});
            expect(Meteor.call).toHaveBeenCalled();
        });
    });
});