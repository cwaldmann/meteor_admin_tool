describe('AdminGroupService', function () {
    'use strict';


    describe('addGroup', function () {
        it('when the name is missing there should be FALSE', function () {
            expect(AdminGroupService.addGroup()).toBe(false);
            expect(AdminGroupService.addGroup('')).toBe(false);
        });


        it('when the name is set a new Group should be insert', function () {
            spyOn(Groups, 'insert');

            AdminGroupService.addGroup('name');
            expect(Groups.insert).toHaveBeenCalled();
        });
    });



    describe('findGroup', function () {
        it('when the ID is missing there should be FALSE', function () {
            expect(AdminGroupService.findGroup()).toBe(false);
        });


        it('when the ID is set the findOne should be called', function () {
            spyOn(Groups, 'findOne');

            AdminGroupService.findGroup('id');
            expect(Groups.findOne).toHaveBeenCalled();
        });
    });





    describe('deleteGroup', function () {
        it('when there is no group there should be FALSE', function () {
            expect(AdminGroupService.deleteGroup()).toBe(false);
        });

        it('when there there are some user in the group the group should not be deleted', function () {
            spyOn(Groups, 'remove');

            AdminGroupService.deleteGroup({
                users: ['user1']
            });

            expect(Groups.remove).not.toHaveBeenCalled();
        });

        it('when there are no more users in a group the group should be deleted', function () {
            spyOn(Groups, 'remove');

            AdminGroupService.deleteGroup({
                users: []
            });

            expect(Groups.remove).toHaveBeenCalled();
        });
    });
});