if (Meteor.isServer) {
    Meteor.methods({
        /**
         * Creates a new User and returns the ID of the user
         *
         * @param username
         * @param password
         * @param email
         * @returns {*|745}
         */
        createNewUser: function (username, password, email) {
            var user = Accounts.createUser({
                username: username,
                password: password,
                email: email
            });

            return user;
        },

        /**
         * Delete User form the system
         *
         * @todo prevent that the admin can delete his self
         *
         * @param id
         * @returns {*|boolean|98|686|3169|5756}
         */
        deleteUser: function (id) {
            return Meteor.users.remove(id);
        }
    });
}

