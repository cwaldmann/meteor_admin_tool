Meteor.startup(function () {

    if (Meteor.users.find().count() === 0) {
        //Create the admin user and some test users
        var admin = Accounts.createUser({
            username: 'admin',
            password: '123',
            email: 'test@test.de'
        });

        Roles.addUsersToRoles(admin, ['admin']);

        //Create some test users
        for (var i = 0; i < 5; i ++) {
            Accounts.createUser({
                username: 'testUser' + i,
                password: '123',
                email: 'test' + i +'@test.de'
            });
        }
    }
});
