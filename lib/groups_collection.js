Groups = new Mongo.Collection('groups');

/**
 * Prevent actions form non admin users
 */
Groups.allow({
    insert: function(userId) {
        return Roles.userIsInRole(Meteor.user(), 'admin');
    },

    remove: function(userId) {
        return Roles.userIsInRole(Meteor.user(), 'admin');
    },

    update: function(userId) {
        return Roles.userIsInRole(Meteor.user(), 'admin');
    }
});