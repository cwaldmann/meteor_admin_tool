/*
 * Account Settings
 */
Accounts.config({
    sendVerificationEmail: false
});


/////////////////////////
// Client
/////////////////////////
if (Meteor.isClient) {
    //Accounts UI Settings
    Accounts.ui.config({
        passwordSignupFields: 'USERNAME_ONLY'
    });
}


/////////////////////////
// Server
/////////////////////////
/**
 * @todo remove return true
 */
if (Meteor.isServer) {
    //Validate that only admin users can add new users
    Accounts.validateNewUser(function () {

        return true;

        //Admin user is created in bootstrap
        if (Meteor.users.find().count() === 0) {
            return true;
        } else {
            var loggedInUser = Meteor.user();
            console.log(loggedInUser);

            if (Roles.userIsInRole(loggedInUser, 'admin')) {
                return true;
            }

            throw new Meteor.Error(403, "Not authorized to create new users");
        }
    });
}


