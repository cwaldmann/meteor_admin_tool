/////////////////////////
// Template and base setup
/////////////////////////
Router.configure({
    layoutTemplate: 'appBody'
});



/////////////////////////
// Route protection
/////////////////////////
var OnBeforeActions = {
    loginRequired: function() {
        if (!Meteor.userId()) {
            this.render('loginForm');
        } else {
            this.next();
        }
    }
};

Router.onBeforeAction(OnBeforeActions.loginRequired, {
    only: ['admin', 'adminUser', 'adminGroup']
});



/////////////////////////
// Route mapping
/////////////////////////
AdminController = RouteController.extend({
    data: function () {
        return {
            users: Meteor.users.find()
        };
    }
});

AdminGroupController = RouteController.extend({
    data: function () {
        return {
            groups: Groups.find({})
        };
    }
});


Router.map(function() {
    this.route('home', {path: '/'});
    this.route('admin', {
            controller: 'AdminController'
        }
    );

    this.route('adminUser', {
        path: '/admin/user',
        controller: 'AdminController'
    });

    this.route('adminGroup', {
        path: '/admin/group',
        controller: 'AdminGroupController'
    });
});