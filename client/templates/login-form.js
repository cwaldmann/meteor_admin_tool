//@todo error handling
Template.loginForm.events({
    'submit .login-form': function(event) {
        event.preventDefault();

        var username = $(event.target).find('[name=username]').val(),
            password = $(event.target).find('[name=password]').val(),
            email = $(event.target).find('[name=email]').val();

        if (username.length > 0 && password.length > 0) {
            Meteor.loginWithPassword(username, password, function (error) {
                if (error) {
                    //error handling
                } else {
                    Router.next();
                }
            });

        } else {
            //error handling
        }
    }
})