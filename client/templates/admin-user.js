AdminUserService = {
    /**
     * Check if the user added useful data
     *
     * @param username
     * @param password
     * @param email
     * @returns {boolean}
     */
    validateUserData: function (username, password, email) {
        var test = false;

        if (username && password && email && username.length > 0 && password.length > 0 && email.length > 0) {
            test = true
        }

        return test;
    },


    /**
     * Add a new User to the system
     *
     * @param username
     * @param password
     * @param email
     * @param callback
     */
    addNewUser: function (username, password, email, callback) {
        if (this.validateUserData(username, password, email)) {
            Meteor.call('createNewUser', username, password, email, callback);
        } else {
            callback(new Error('Please fill the mandatory fields'));
        }
    },

    /**
     * Deletes a user
     *
     * @param id
     * @param callback
     */
    deleteUser: function (id, callback) {
        if (id && callback) {
            Meteor.call('deleteUser', id, callback);
        } else {
            //error handling
        }

    }
}



Template.adminUser.events({
    /**
     * @todo error handling
     * @todo succces message
     *
     * @param event
     */
    'submit .new-user': function(event) {
        event.preventDefault();

        var username = $(event.target).find('[name=username]').val(),
            password = $(event.target).find('[name=password]').val(),
            email = $(event.target).find('[name=email]').val();

        AdminUserService.addNewUser(username, password, email, function (err, response) {
            //error or succes message handling
            console.log(err);
        });
    },

    /**
     * Removes a user
     *
     * @todo error handling
     * @todo confirm dialog
     *
     * @param event
     */
    'click .user-delete-link': function(event) {
        event.preventDefault();

        var id = $(event.target).data('user-id');

        AdminUserService.deleteUser(id, function (err, response) {
            //error or succes message handling
            console.log(err);
        });
    }
})


