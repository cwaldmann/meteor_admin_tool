AdminGroupService = {
    /**
     * Returns a Group Object
     * @param id
     * @returns {*}
     */
    findGroup: function(id) {
        if (id) {
            return Groups.findOne({
                _id: id
            });
        }

        return false;
    },

    /**
     * Removes Group if the group has no users
     *
     * @param group
     * @returns {*}
     */
    deleteGroup: function(group) {
        if (group && (!group.users || group.users.length < 1)) {
            return Groups.remove(group._id)
        }

        return false;
    },

    /**
     * Adds a group
     *
     * @param name
     * @returns {*}
     */
    addGroup: function(name) {
        if (name && name.length > 0) {
            return Groups.insert({
                name: name,
                users: []
            });
        }

        return false;
    }
}


Template.adminGroup.events({
    /**
     * Listen for the submit event
     * of the add group form
     *
     * @todo error handling
     * @todo succces message
     *
     * @param event
     */
    'submit .new-group': function(event) {
        event.preventDefault();

        var name = $(event.target).find('[name=groupname]').val(),
            result = AdminGroupService.addGroup(name);

        if (result) {
            //successmessage
        } else {
           //error handling
        }
    },

    /**
     * Listen for the group-delete-link
     * click Event.
     *
     * @todo error handling
     * @todo confirm dialog
     *
     * @param event
     */
    'click .group-delete-link': function(event) {
        event.preventDefault();

        var id = $(event.target).data('group-id'),
            group = AdminGroupService.findGroup(id),
            result = AdminGroupService.deleteGroup(group);

        if (result) {
            //successmessage
        } else {
            //error handling
        }
    }
})


