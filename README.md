# Meteor Experiment
Simple Project to learn Meteor. [Meteor](https://www.meteor.com/install) is is an open-source JavaScript web application framework and environment written in Node.js.

This project is based on some simple stories like:

  * 'As an admin I can add users. A user has a name'
  * 'As an admin I can delete users'
  * ...

## To launch the application please
  * Install [meteor](https://www.meteor.com/install)
  * Open your terminal, go to this folder and enter ```$ meteor```
  * Open http://localhost:3000/ in your browser
  * You can login with the user 'admin' and the password '123'



## Tests
After the application is up, you can run the tests:

  * Please open Google Chrome
  * and open http://localhost:52672/

The test were written with jasmine and are running with karma.
To see the test results open http://localhost:3000/ and click on the icon in the right corner.


## artifact
For the artifacts, please have a look in the artifact folder of this repository.

  * (a) Domain model: There is lots of magic inside the framework which is not outlined in this model
  * (c) Meteor is build on an "Isomorphic APIs". This mean that the same code, written in the same language, can run on both client and server.
  * (d) Perhaps a bit provocative in this situation, but to build a nice and working interface you need a team of really good UX/UI Designers and programmers who work hand in hand from the beginning of the project.
